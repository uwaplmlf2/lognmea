// Lognmea logs NMEA data to a file
package main

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"text/template"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
	"github.com/tarm/serial"
)

const Usage = `Usage: lognmea [options] serialdev [duration]

Log data records from an NMEA device connected to serialdev for
the specified duration or until interrupted. The output is sent
to stdout unless changed with the -o option.

`

const TS_FORMAT = "2006-01-02T15:04:05.999Z07:00"

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	serialBaud int = 38400
	rdTimeout  time.Duration
	outFile    string
	addTime    bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.IntVar(&serialBaud, "baud", serialBaud,
		"serial baud rate")
	flag.DurationVar(&rdTimeout, "timeout", rdTimeout,
		"set read timeout")
	flag.StringVar(&outFile, "o", outFile,
		"send output to named file")
	flag.BoolVar(&addTime, "add-time", addTime,
		"add timestamp to each record")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func readData(ctx context.Context, rdr io.Reader, tlimit time.Duration) <-chan nmea.Sentence {
	ch := make(chan nmea.Sentence, 1)
	rbuf := bufio.NewReader(rdr)
	go func() {
		defer close(ch)
		if tlimit > 0 {
			var cancel context.CancelFunc
			ctx, cancel = context.WithTimeout(ctx, tlimit)
			defer cancel()
		}

		for {
			s, err := nmea.ReadSentence(rbuf)
			if err != nil {
				log.Printf("%v", err)
				if errors.Is(err, os.ErrDeadlineExceeded) {
					return
				}
				continue
			}

			if s.IsEmpty() {
				continue
			}

			select {
			case ch <- s:
			case <-ctx.Done():
				return
			default:
				log.Printf("NMEA queue full, %q message dropped", s.Id)
			}
		}
	}()

	return ch
}

type timeInfo struct {
	time.Time
}

func (t timeInfo) Ts() string {
	const timefmt = "20060102_150405"
	return t.Format(timefmt)
}

func (t timeInfo) Yday() string {
	return fmt.Sprintf("%03d", t.YearDay())
}

func (t timeInfo) Mday() string {
	return fmt.Sprintf("%02d", t.Day())
}

func (t timeInfo) Mon() string {
	return fmt.Sprintf("%02d", int(t.Month()))
}

func nameFromTemplate(text string, tref time.Time) string {
	t := timeInfo{Time: tref}
	tmpl, err := template.New("test").Parse(text)
	if err != nil {
		log.Fatalf("Invalid filename template %q: %v", text, err)
	}
	var buf bytes.Buffer
	tmpl.Execute(&buf, t)
	return buf.String()
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		duration time.Duration
		err      error
	)

	if len(args) > 1 {
		duration, err = time.ParseDuration(args[1])
		if err != nil {
			log.Fatalf("Invalid duration %q: %v", args[1], err)
		}
	}

	p, err := serial.OpenPort(&serial.Config{
		Name:        args[0],
		Baud:        serialBaud,
		ReadTimeout: rdTimeout,
	})
	if err != nil {
		log.Fatalf("Cannot open %q: %v", args[0], err)
	}
	defer p.Close()

	var fout io.WriteCloser = os.Stdout
	if outFile != "" {
		outFile = nameFromTemplate(outFile, time.Now().UTC())
		fout, err = os.Create(outFile)
		if err != nil {
			log.Fatalf("Cannot open %q: %v", outFile, err)
		}
	}
	defer fout.Close()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ch := readData(ctx, p, duration)
	for {
		select {
		case sig := <-sigs:
			log.Printf("Exiting on signal: %v", sig)
			return
		case s, ok := <-ch:
			if !ok {
				log.Printf("No more data from %s", args[0])
				return
			}
			if addTime {
				fmt.Fprint(fout, time.Now().UTC().Format(TS_FORMAT)+",")
			}
			fmt.Fprintln(fout, s.String())
		}
	}

}

module bitbucket.org/uwaplmlf2/lognmea

go 1.17

require (
	bitbucket.org/mfkenney/go-nmea v1.5.2
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect

#!/usr/bin/env bash
#
# Upload a file to the Downloads section of a Bitbucket
# repository. Authentication is via an app password which you must create at
# the link below:
#
#    https://bitbucket.org/account/settings/app-passwords/
#
# The following env variables must be set:
#
#    BITBUCKET_USERNAME=yourusername
#    BITBUCKET_APP_PASSWORD=passwordcreatedabove
#
# You can also set the above variables in a file and pass the file to this
# script using the -E option:
#
#    bbupload.sh -E envfile file_to_upload
#
# Optional variables
#
#    BITBUCKET_WORKSPACE=workspacename
#    BITBUCKET_REPO_SLUG=reponame
#
# If unset, these values will be extract from the URL for the git remote
# "origin" repository.
#
[[ "$1" = -E ]] && {
    shift
    source "$1"
    shift
}

if [[ -z "$1" ]]; then
    echo "Missing filename" 1>&2
    exit 1
fi

if [[ -z $BITBUCKET_USERNAME ]] || [[ -z $BITBUCKET_APP_PASSWORD ]]; then
    echo "Missing upload credentials" 1>&2
    exit 1
fi

set -e

BB_AUTH_STRING="${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}"

if [[ -z $BITBUCKET_WORKSPACE ]] || [[ -z $BITBUCKET_REPO_SLUG ]]; then
    # Extract the workspace and repo name from the git remote
    remote=$(git remote get-url origin)
    path=$(cut -f2 -d: <<< "$remote")
    BITBUCKET_WORKSPACE="${path%/*}"
    slug="${path##*/}"
    # Trim the .git extension
    BITBUCKET_REPO_SLUG="${slug%.*}"
fi

url="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/downloads"

curl -s -S -X POST --user "${BB_AUTH_STRING}" "$url" --form files=@"$1"

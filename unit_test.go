package main

import (
	"testing"
	"time"
)

func TestFileTemplate(t *testing.T) {
	tref := time.Date(2001, time.January, 1, 12, 34, 0, 0, time.UTC)
	tests := []struct {
		in, out string
	}{
		{
			in:  "foo_{{.Ts}}.txt",
			out: "foo_20010101_123400.txt",
		},
		{
			in:  "file_{{.Year}}_{{.Yday}}.txt",
			out: "file_2001_001.txt",
		},
	}

	for _, tc := range tests {
		result := nameFromTemplate(tc.in, tref)
		if result != tc.out {
			t.Errorf("Check failed; expected %q, got %q", tc.out, result)
		}
	}
}
